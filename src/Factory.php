<?php
/*
 * This file is part of the binfinn/template.
 *
 * (c) Shakiusa <2244137744@qq.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace BinFinn\Template;

/**
 * Class Factory.
 *
 * @method static \BinFinn\Template\Example\Application            example(array $config)
 */
class Factory
{
    /**
     * @param string $name
     *
     * @return \BinFinn\Template\Kernel\ServiceContainer
     */
    public static function make($name, array $config)
    {
        $namespace = ucwords($name);
        $application = "\\BinFinn\\Template\\{$namespace}\\Application";

        return new $application($config);
    }

    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}
