<?php
/*
 * This file is part of the binfinn/template.
 *
 * (c) Shakiusa <2244137744@qq.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace BinFinn\Template\Example\NewDemo;

/**
 * Class Client
 * @package BinFinn\Template\Example\NewDemo
 */
class Client
{
    /**
     * change the first word to upper.
     */
    public function test(string $params)
    {
        return ucfirst($params);
    }

}
