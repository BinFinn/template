<?php
/*
 * This file is part of the binfinn/template.
 *
 * (c) Shakiusa <2244137744@qq.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace BinFinn\Template\Example;

use BinFinn\Template\Example\Demo\ServiceProvider;
use BinFinn\Template\Kernel\ServiceContainer;

/**
 * Class Application
 * @package BinFinn\Template\Example
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Demo\ServiceProvider::class,
        NewDemo\ServiceProvider::class
    ];

    /**
     * @var array
     */
    protected $defaultConfig = [];

    /**
     * @param string $method
     * @param array $arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        // return call_user_func_array([$this['base'], $method], $arguments);
        // return $this->base->$method(...$arguments);
        return $this['base']->$method(...$arguments);
    }
}
