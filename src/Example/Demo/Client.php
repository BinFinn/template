<?php
/*
 * This file is part of the binfinn/template.
 *
 * (c) Shakiusa <2244137744@qq.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace BinFinn\Template\Example\Demo;

/**
 * Class Client
 * @package BinFinn\Template\Example\Demo
 */
class Client
{
    /**
     * @desc renturn array params anyway.
     */
    public function test(array $params)
    {
        return $params;
    }

}
