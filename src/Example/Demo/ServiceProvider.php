<?php
/*
 * This file is part of the binfinn/template.
 *
 * (c) Shakiusa <2244137744@qq.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace BinFinn\Template\Example\Demo;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * Class ServiceProvider
 * @package BinFinn\Template\Example\Demo
 */
class ServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}.
     */
    public function register(Container $app)
    {
        $app['demo'] = function ($app) {
            return new Client($app);
        };
    }
}
