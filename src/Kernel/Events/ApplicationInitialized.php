<?php
/*
 * This file is part of the binfinn/template.
 *
 * (c) Shakiusa <2244137744@qq.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace BinFinn\Template\Kernel\Events;

use BinFinn\Template\Kernel\ServiceContainer;

/**
 * Class ApplicationInitialized
 * @package BinFinn\Template\Kernel\Events
 */
class ApplicationInitialized
{
    /**
     * @var \BinFinn\Template\Kernel\ServiceContainer
     */
    public $app;

    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
    }
}
