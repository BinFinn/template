## 一个优秀的SDK开发包: binfinn/template

📦 用于开发Composer包的SDK。内置两个示例，可以无限拓展其他功能。主要用于三方平台SDK调用封装及自身业务封装。是一款简单易用易上手的辅助开发包。

## Documentation

[官网](敬请期待...)  · [教程](敬请期待...)

### Requirement

1. PHP >= 7.0
2. **[Composer](https://getcomposer.org/)**


### Installation

```shell
$ composer require "binfinn/template" -vvv
```

### Usage

基本使用:

```php
<?php

use BinFinn\Template\Example\Application;

$config = [
    'token' => 'token value',
    'key' => 'key value',
    'action' => 'action value'
];

$app = new Application($config);
$demo = $app->demo;
$new_demo = $app->new_demo;
// 调用方法
echo json_encode($app->getConfig()) . "<br>";
echo json_encode($demo->test($config)) . "<br>";
echo json_encode($new_demo->test("abcdefg")) . "<br>";
exit();

```

```php
<?php

use BinFinn\Template\Factory;

$config = [
    'token' => 'token value',
    'key' => 'key value',
    'action' => 'action value'
];

$app = Factory::example($config);
$demo = $app->demo;
$new_demo = $app->new_demo;
// 调用方法
echo json_encode($app->getConfig()) . "<br>";
echo json_encode($demo->test(['aa', 'bb', 'cc'])) . "<br>";
echo json_encode($new_demo->test("abcdefg")) . "<br>";
exit();

```




##### 技术参考(部分):

https://segmentfault.com/a/1190000014471794

https://dryyun.com/2018/04/17/php-pimple/

