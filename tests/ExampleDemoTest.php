<?php

declare(strict_types=1);

namespace BinFinn\Template;

use BinFinn\Template\Factory;
use BinFinn\Template\Example\Demo;
use PHPUnit\Framework\TestCase;

class ExampleDemoTest extends TestCase
{

    /**
     * 使用方法：
     *
     * $config = [
     * 'token'  => 'token value',
     * 'key'    => 'key value',
     * 'action' => 'action value'
     * ];
     *
     * // @TODO method1: 实例化 Application.
     * $app = new \BinFinn\Template\Example\Application($config);
     * echo json_encode($app->getConfig()) . "<br>";
     * // 调用方法
     * echo json_encode($app->demo->test(['aa', 'bb', 'cc'])) . "<br>";
     * echo json_encode($app->new_demo->test("abcdefg")) . "<br>";
     * exit();
     *
     * // @TODO method2: 静态注册Example
     * $this->app = Factory::example($config);
     * echo json_encode($this->app->getConfig()) . "<br>";
     * // 调用方法
     * echo json_encode($this->app->demo->test(['aa', 'bb', 'cc'])) . "<br>";
     * echo json_encode($this->app->new_demo->test("abcdefg")) . "<br>";
     * exit();
     */

    public function test1()
    {
        $config = [
            'token' => 'token value',
            'key' => 'key value',
            'action' => 'action value'
        ];

        // @TODO method1: 实例化 Application.
        $app = new \BinFinn\Template\Example\Application($config);
        echo json_encode($app->getConfig()) . "<br>";
        // 调用方法
        echo json_encode($app->demo->test(['aa', 'bb', 'cc'])) . "<br>";
        echo json_encode($app->new_demo->test("abcdefg")) . "<br>";
        exit();

    }

    public function test2()
    {
        $config = [
            'token' => 'token value',
            'key' => 'key value',
            'action' => 'action value'
        ];


        // @TODO method2: 静态注册Example
        $this->app = Factory::example($config);
        echo json_encode($this->app->getConfig()) . "<br>";
        // 调用方法
        echo json_encode($this->app->demo->test(['aa', 'bb', 'cc'])) . "<br>";
        echo json_encode($this->app->new_demo->test("abcdefg")) . "<br>";
        exit();
    }


}
